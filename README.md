# Türkçe Linux bash

## Açıklama

Türkçe bashi kullanarak günlük hayatta sık kullanılan(program kurma gibi) bash komutlarını türkçe olarak kullana bilirsiniz. Komutlar Debian, Ubuntu, Mint, Pardus ve diğer Debian tabanlı dağıtımlarda geçerlidir.

## Kurulumu

### Dosyalarımız indirip uygun konuma taşıyalım:

```bash
git clone https://gitlab.com/linux-is-not-unix/turkish-bash.git
cd turkish-bash
```

### türkçe-bash.txt dosyasını açalım:

```bash
sudo nano türkçe-bash.txt
```

Dosyanın içerisindeki her şeyi Ctrl+Shift+A, Ctrl+Shift+C aracılığıyla koplyayayım.

### bashrc dosyasını editleyeylim:

```bash
cd
sudo nano .bashrc
```

Dosyanın sonuna gelip az önce kopyaladığımız verileri buraya Ctrl+Shift+V aracılığıyla yapışıtralım. Ctrl+O, Crt+X aracılığıyla dosyayı kaydedelim.

## Kullanım

Türkçe bashte bulunan komutlara bi göz atalım

### Komutlar:

Root yetkisi alma

```bash
kök
```

Depoyu güncelleme

```bash
depo-güncelle
```

Kurulu tüm paketleri güncelleme

```bash
paket-güncelle
```

Sistemi güncelleme

```bash
sistem-güncelle
```

Program kurma

```bash
kur
```

Program kaldırma

```bash
kaldır
```

Depoda program arama

```bash
ara
```

Dosya oluşturma

```bash
oluştur
```

Dosya Taşıma

```bash
taşı
```

Dosya kopyalama

```bash
kopyala
```

Dosya silme

```bash
sil
```

Dosyaları listeleme

```bash
liste
```

Terminali kapatma 

```bash
çık
```

Konum değiştirme

```bash
kd
```

Yazı yazdırma

```bash
yaz
```

